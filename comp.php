<?php 
	require 'config.php';
	require 'class/racc.php';
	require '../app/Mage.php';
	require 'class/core.php';
	umask(0);
	Mage::app();
	
	
	$configData = Mage::getModel('core/config_data')->getCollection();

	if ($_SERVER['REQUEST_METHOD'] == 'POST') {		
		$branchName = $_POST['brachname'];
		$aDoor = $_POST['chbupdate'];
		$N = count($aDoor);
		if ($branchName && $N > 0) {
			
			$beforeFile = genUniqueFileName('before_'.$branchName, "xmlfiles/", "xml");
			$beforeXML = tableToXML($configData);
			$handle = fopen($beforeFile, "w");
			if (writeToFile($handle, $beforeXML)) {		
				if(empty($aDoor)) 
				{
					echo("You didn't select any records.");
				} else {
					for($i=0; $i < $N; $i++)						
					{							
						$store 	= ($_POST[$aDoor[$i].'_data_store']);
						$storeid 	= ($_POST[$aDoor[$i].'_data_storeid']);
						$path 		= ($_POST[$aDoor[$i].'_data_path']);
						$ovalue 	= ($_POST[$aDoor[$i].'_data_ovalue']);
						$nvalue 	= ($_POST[$aDoor[$i].'_data_nvalue']);
						$status 	= ($_POST[$aDoor[$i].'_data_status']);	
						
						if ($status == 'diff') {
							Mage::getConfig()->saveConfig($path, $nvalue, $store, $storeid);
							echo "<div>Updated</div>";					
						} elseif ($mystatus == 'remove') { 
							echo "<div>It is not safe to remove.</div>";					
						}elseif ($mystatus == 'new') { 
							echo "<div>Install the extension.</div>";					
						}
					}
				}
				
				$configData = Mage::getModel('core/config_data')->getCollection();
				$afterXML = tableToXML($configData);
				$afterFile = genUniqueFileName('after_'.$branchName, "xmlfiles/", "xml");
				$handle = fopen($afterFile, "w");				
				writeToFile($handle, $afterXML);
					
				echo "Successfully imported the selected changes";
				
				 $logFile = "logs/logs.xml";
				 $xml = simplexml_load_file($logFile) or die("Error: Cannot create object");
				 $xml = getLogXML($xml, "Import", $afterFile, $beforeFile, $branchName);
				 $xml->saveXML($logFile);
			} else {
				echo "Error creating the backup. Did not make any changes";
			}			
		} else {
			echo "Please enter valid branch name.";
		}
	} 
?>
<html>
	<head>
		<title>Magento Import Config Table</title>
		<link href="css/style.css" rel="stylesheet" type="text/css"/>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
		<script src="js/script.js"></script>
	</head>
	<body>	
		<section class="header">
		<div class="page-row title"> 
			<h1>Magento Import Config Table</h1>
		</div>
		<div class="page-row control-panel">
			<ul>				
				<li class="upl"><a href="<?php echo BASEURL;?>">Back</a></li>
				<li class="imp active">Compare and Import</li>					
			</ul>
		</div>
		</section>
		<section class="body">
			<div id="importchanges" class="column half tabbody active">
				<h2>Import Selected Changes</h2>
				<div class="form-body">
					<form action="" method="POST" id ="impform">
					<div class="element"><label for="brachname">Branch/Backup Name:</label><input type="textbox" name="brachname" id="brachname" /></div>
					<div class="element submit"><button type="submit"  class="btn sync">Import Selected Changes</button></div>
					<div class="result">
					<?php 	
					$forg_file = htmlspecialchars($_GET["file"]);	//file to be compared				

					$mastFileArray = [];   //Diff results
					$homeFileArray = [];   //Current config data to array
					$forgFileAyyay = [];   //Import file to array 					

					$homeFileArray = tableToArray($configData);
					
					$xml = simplexml_load_file($forg_file) or die("Error: Cannot create object");
					$forgFileAyyay = xmlToArray($xml);

					$mastFileArray = compareTwoConfig($homeFileArray, $forgFileAyyay);
					?>
						<div class="table">
							<div class="row head">
								<div class="column select"></div>
								<div class="column id">Id</div>
								<div class="column store">Store</div>
								<div class="column storeid">Store Id</div>
								<div class="column path">Path</div>
								<div class="column ovalue">Original Value</div>
								<div class="column nvalue">New Value</div>
								<div class="column status">Status</div>
								<div class="column update">Update</div>
							</div>
							<?php foreach ($mastFileArray as $ma) { ?>
								<div class="row">
									<div class="column select">
										<input type="checkbox" class="isupdate" name="chbupdate[]" value="<?php echo $ma["id"];?>" />
										<input type="hidden" name="<?php echo $ma['id'];?>_data_store" 		value="<?php echo $ma["store"]; ?>" />
										<input type="hidden" name="<?php echo $ma['id'];?>_data_storeid"  	value="<?php echo $ma["storeid"]; ?>" />
										<input type="hidden" name="<?php echo $ma['id'];?>_data_path"   	value="<?php echo $ma["path"]; ?>" />
										<input type="hidden" name="<?php echo $ma['id'];?>_data_ovalue"   	value="<?php echo $ma["originalvalue"]; ?>" />
										<input type="hidden" name="<?php echo $ma['id'];?>_data_nvalue"   	value="<?php echo $ma["newvalue"]; ?>" />
										<input type="hidden" name="<?php echo $ma['id'];?>_data_status"  	value="<?php echo $ma["status"]; ?>" />
									</div>
									<div class="column id"><?php echo $ma["id"]; ?></div>
									<div class="column store"><?php echo $ma["store"]; ?></div>
									<div class="column storeid"><?php echo $ma["storeid"]; ?></div>
									<div class="column path"><?php echo $ma["path"]; ?></div>
									<div class="column ovalue"><?php echo $ma["originalvalue"]; ?></div>
									<div class="column nvalue"><?php echo $ma["newvalue"]; ?></div>
									<div class="column status"><?php echo $ma["status"]; ?></div>
									<div class="column update"><span id="status_<?php echo $ma["id"];?>"></span></div>
								</div>	
							<?php } ?>																				
						</div>
					</div>
					<div class="result-table">
						<div id="update-status"></div>
						<div id="compare-table"></div>
					</div>
					</form>
				</div>
			</div>
		</section>
</div>
</body>	
</html>