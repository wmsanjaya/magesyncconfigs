<?php 
	require 'class/racc.php';	
	require 'config.php';
	require 'class/core.php';
?>
<html>
	<head>
		<title>Magento Import Config Table</title>
		<link href="css/style.css" rel="stylesheet" type="text/css"/>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
		<script src="js/script.js"></script>
	</head>
	<body>	
		<section class="header">
			<div class="page-row title"> 
				<h1>Magento Import Config Table</h1>
			</div>
			<div class="page-row control-panel">
				<ul>				
					<li class="upl"><a href="<?php echo BASEURL;?>">Back</a></li>
					<li class="imp active">History</li>					
				</ul>
			</div>
		</section>
		<section class="body">
			<div id="importchanges" class="column half tabbody active">
				<?php 
				$logs = getHistory(); ?>
				<div class="table">
					<div class="row head">								
						<div class="column action">Action</div>
						<div class="column date-time">Date/Time</div>
						<div class="column before">Before Update</div>
						<div class="column after">After Update</div>
						<div class="column branch">Branch</div>								
					</div>
					<?php foreach ($logs as $log) { ?>
						<div class="row">									
							<div class="column action"><?php echo $log[0]; ?></div>
							<div class="column date-time"><?php echo $log[1]; ?></div>
							<div class="column before"><?php echo $log[2]; ?></div>
							<div class="column after"><?php echo $log[3]; ?></div>
							<div class="column branch"><?php echo $log[4]; ?></div>
							<div class="column reset"><a href="<?php echo BASEURL."/comp.php?file=".$log[2];?>">Compare and revert</a></div>
						</div>	
					<?php } ?>	
				</div>
			</div>
		</section>
	</body>	
</html>