<?php 
require '../../app/Mage.php';
umask(0);
Mage::app();

$mybranch 	= $_POST["ibranch"];
$mycid   	= $_POST["icid"];
$mystore 	= $_POST["istore"];
$storeid 	= $_POST["istoid"];
$mypath	 	= $_POST["ipath"];
$oldval	 	= $_POST["ioval"];
$newval  	= $_POST["inval"];
$mystatus  	= $_POST["istatus"];

if ($mystatus == 'diff') {
	Mage::getConfig()->saveConfig($mypath, $newval, $mystore, $storeid);
	echo "<span>Updated</span>";
	return true;
} elseif ($mystatus == 'remove') { 
	echo "<span>It is not safe to remove.</span>";
	return false;
}elseif ($mystatus == 'new') { 
	echo "<span>Install the extension.</span>";
	return false;
}