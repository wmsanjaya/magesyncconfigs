<?php 	
$home_file = "../".$_POST["homef"];
$forg_file = "../".$_POST["forif"];

$mastFileArray = [];
$homeFileArray = [];
$forgFileAyyay = [];

require '../../app/Mage.php';
umask(0);
Mage::app();

$configData = Mage::getModel('core/config_data')->getCollection();

foreach ($configData as $config) {
	$stringData = $config->getId()."\t".$config->getScope()."\t".$config->getScopeId()."\t".$config->getPath()."\t".$config->getValue()."\n";
	array_push ($homeFileArray, array($config->getId(),$config->getScope(),$config->getScopeId(), $config->getPath(),$config->getValue()));
}

$xml=simplexml_load_file($forg_file) or die("Error: Cannot create object");
foreach ($xml as $rows) {
	$tempArray = []; 
	foreach((array)$rows as $k => $val) {	
		array_push ($tempArray, $val);
	}
	array_push ($forgFileAyyay, $tempArray);
}

$mastFileArray = compareTwoConfig($homeFileArray, $forgFileAyyay);
?>
<div class="table">
	<div class="row head">
		<div class="column select"></div>
		<div class="column id">Id</div>
		<div class="column store">Store</div>
		<div class="column storeid">Store Id</div>
		<div class="column path">Path</div>
		<div class="column ovalue">Original Value</div>
		<div class="column nvalue">New Value</div>
		<div class="column status">Status</div>
		<div class="column update">Update</div>
	</div>
<?php foreach ($mastFileArray as $ma) { ?>
	<div class="row">
		<div class="column select">
			<input type="checkbox" class="isupdate" name="update_<?php echo $ma["id"];?>" value="<?php echo $ma["id"];?>"							
			   data-store="<?php echo $ma["store"]; ?>"
			   data-storeid="<?php echo $ma["storeid"]; ?>"
			   data-path="<?php echo $ma["path"]; ?>"
			   data-ovalue="<?php echo $ma["originalvalue"]; ?>"
			   data-nvalue="<?php echo $ma["newvalue"]; ?>"
			   data-status="<?php echo $ma["status"]; ?>"
			/>
		</div>
		<div class="column id"><?php echo $ma["id"]; ?></div>
		<div class="column store"><?php echo $ma["store"]; ?></div>
		<div class="column storeid"><?php echo $ma["storeid"]; ?></div>
		<div class="column path"><?php echo $ma["path"]; ?></div>
		<div class="column ovalue"><?php echo $ma["originalvalue"]; ?></div>
		<div class="column nvalue"><?php echo $ma["newvalue"]; ?></div>
		<div class="column status"><?php echo $ma["status"]; ?></div>
		<div class="column update"><span id="status_<?php echo $ma["id"];?>"></span></div>
	</div>	
<?php } ?>
</div>
<?php
	function compareTwoConfig($as, $bs){
		$masterArray = [];
		foreach ($as as $ia=>$a ) {
			foreach ($bs as $ib=>$b) {
				if ($a[1] == $b[1] && $a[2] == $b[2] && $a[3] == $b[3]){				
					if(trim($a[4]) != trim($b[4])) {						
						array_push ($masterArray, array("id" => $a[0],"store" => $a[1],"storeid"=>$a[2], "path"=>$a[3],"originalvalue"=>$a[4],"newvalue"=>$b[4],"status"=>"diff"));
						unset($as[$ia]);
						unset($bs[$ib]);						
					} else {						
						unset($as[$ia]);
						unset($bs[$ib]);
					}
				}
			}
		}
						
		foreach ($bs as $ib=>$b) {
			array_push ($masterArray, array("id" => $a[0],"store" => $a[1],"storeid"=>$a[2], "path"=>$a[3],"originalvalue"=>"","newvalue"=>$b[4],"status"=>"new"));	
		}
		
		foreach ($as as $ia=>$a) {
			array_push ($masterArray, array("id" => $a[0],"store" => $a[1],"storeid"=>$a[2], "path"=>$a[3],"originalvalue"=>$a[4],"newvalue"=>"","status"=>"remove"));	
		}
		
		return $masterArray;
	}
	
	
	function _pr($a) {
		echo "<pre>";
		print_r($a);
		echo "</pre>";
	}

	function getDiffArray($diff) {
		return explode("	",$diff);
	}
?>