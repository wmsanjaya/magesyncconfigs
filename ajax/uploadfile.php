<?php
require '../class/core.php';
require '../config.php';
$target_dir = "../xmlfiles/";
$target_file = $target_dir . 'upload_'.basename($_FILES["file"]["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

if (! file_exists($target_file)) {
	if (move_uploaded_file($_FILES['file']['tmp_name'], $target_file)) { 
		echo "<span class='msg success'>File Uploaded.</span>";
		updateLog("Upload", $target_file, $target_file, "upload");
	} else {
		echo "<span class='msg error'>Error uploading the file.</span>";
	}
} else {
	echo "<span class='msg warning'>File already exists.</span>";
}
