<?php 
	require '../../app/Mage.php';
	require '../class/core.php';
	require '../config.php';
	umask(0);
	Mage::app();
	
	$magemodel = $_POST["magemodel"];	
	$filename = genUniqueFileName("export_".$magemodel."_".$_POST["forif"],'/xmlfiles/','xml');		
	$dwFile = $filename;
	$xmlRoot = "root";
	
    if ($magemodel == "coreconf"):	
		$mageData = Mage::getModel('core/config_data')->getCollection();
		$xmlRoot  = "core_config_data";
	elseif($magemodel == "products"):
		$mageData = Mage::getModel('catalog/product')->getCollection();
		$xmlRoot  = "catalog_product";
	elseif($magemodel == "category"):
		$mageData = Mage::getModel('catalog/category')->getCollection();
		$xmlRoot  = "catalog_category";
	else:
		echo "Please specify model";
		return true;
	endif;
	
		
	$doc  = new DomDocument('1.0', 'ISO-8859-1');
	$doc->formatOutput   = true;
    $root = $doc->createElement("core_config_data");
    $root = $doc->appendChild($root);
	
	foreach ($mageData as $data) {
		$container = $doc->createElement("row");
		$data = $data->load($data->getId())->getData();
		
		foreach($data as $key => $value){
			$child = $doc->createElement($key);
			$child = $container->appendChild($child);
			$valuen = $doc->createTextNode($value);
			$valuen = $child->appendChild($valuen);
		}
		$root->appendChild($container);
	}	
	$strxml = $doc->saveXML();
	
	$handle = fopen(ROOT.$dwFile, "w");
	fwrite($handle, $strxml);
	fclose($handle);
	
	updateLog("Export", $dwFile, $dwFile, $_POST["forif"]);
	
	echo $dwFile;	
?>