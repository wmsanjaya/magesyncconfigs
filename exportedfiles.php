<?php 
	require 'class/racc.php';	
	require 'config.php';
	require 'class/core.php';
?>
<html>
	<head>
		<title>Magento Import Config Table</title>
		<link href="css/style.css" rel="stylesheet" type="text/css"/>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
		<script src="js/script.js"></script>
	</head>
	<body>	
		<section class="header">
			<div class="page-row title"> 
				<h1>Magento Import Config Table</h1>
			</div>
			<div class="page-row control-panel">
				<ul>				
					<li class="upl"><a href="<?php echo BASEURL;?>">Back</a></li>
					<li class="imp active">Exported Files</li>					
				</ul>
			</div>
		</section>
		<section class="body">
			<div id="importchanges" class="column half tabbody active">
				Exported files
				<?php 
					foreach (glob("xmlfiles/export_*.xml") as $filename) {
				?>
					<div class="exportedfile">
						<span><?php echo $filename;?></span>
						<a href='<?php echo BASEURL.'/'.$filename;?>' download='<?php echo $filename;?>'>Click here to download. </a>
					</div>
				<?php }
				?>
			</div>
		</section>
	</body>	
</html>