<?php

function updateLog($action, $afile, $bfile, $branch){
	$xml = simplexml_load_file(LOG_XML) or die("Error: Cannot create object");
	$xml = getLogXML($xml, $action, $afile, $bfile, $branch);
	$xml->saveXML(LOG_XML);
}

function getHistory() {	
	$xml=simplexml_load_file(HISTORY_XML) or die("Error: Cannot create object");
	return xmlToArray($xml);
}


function getLogXML($xml, $action, $afile, $bfile, $branch) {
	$log = $xml->addChild("record");	
	$log->addChild("action", $action);
	$log->addChild("date", date('m-d-Y hia'));
	$log->addChild("beforefile", $bfile );
	$log->addChild("afterfile", $afile );
	$log->addChild("branch", $branch );
	return $xml;
}

function writeToFile($handle, $content) {	
	fwrite($handle, $content);
	fclose($handle);
	return true;	
}

function genUniqueFileName($prefix, $path, $ext) {
	return $path.$prefix."_".date('m_d_Y_hia').".".$ext;	
}

function tableToArray($configData) {
	$tableArray = []; 
	foreach ($configData as $config) {
		$tempArray = []; 
		foreach($config->getData() as $key => $val){
			array_push ($tempArray, $val);
		}
		array_push ($tableArray, $tempArray);
	}	
	return $tableArray;
}

function tableToXML($configData) {
	$doc  = new DomDocument('1.0', 'ISO-8859-1');
	$doc->formatOutput   = true;
    $root = $doc->createElement("core_config_data");
    $root = $doc->appendChild($root);
	
	foreach ($configData as $config) {
		$container = $doc->createElement("row");
		foreach($config->getData() as $key => $value){
			$child = $doc->createElement($key);
			$child = $container->appendChild($child);
			$valuen = $doc->createTextNode($value);
			$valuen = $child->appendChild($valuen);
		}
		$root->appendChild($container);
	}	
	return $strxml = $doc->saveXML();
}


function xmlToArray($xml) {
	$xmlArray = [];
	
	foreach ($xml as $rows) {
		$tempArray = []; 
		foreach((array)$rows as $k => $val) {	
			array_push ($tempArray, $val);
		}
		array_push ($xmlArray, $tempArray);
	}
	return $xmlArray;
}

function compareTwoConfig($as, $bs){
	$masterArray = [];
	foreach ($as as $ia=>$a ) {
		foreach ($bs as $ib=>$b) {
			if ($a[1] == $b[1] && $a[2] == $b[2] && $a[3] == $b[3]){				
				if(trim($a[4]) != trim($b[4])) {						
					array_push ($masterArray, array("id" => $a[0],"store" => $a[1],"storeid"=>$a[2], "path"=>$a[3],"originalvalue"=>$a[4],"newvalue"=>$b[4],"status"=>"diff"));
					unset($as[$ia]);
					unset($bs[$ib]);						
				} else {						
					unset($as[$ia]);
					unset($bs[$ib]);
				}
			}
		}
	}
					
	foreach ($bs as $ib=>$b) {
		array_push ($masterArray, array("id" => $a[0],"store" => $a[1],"storeid"=>$a[2], "path"=>$a[3],"originalvalue"=>"","newvalue"=>$b[4],"status"=>"new"));	
	}
	
	foreach ($as as $ia=>$a) {
		array_push ($masterArray, array("id" => $a[0],"store" => $a[1],"storeid"=>$a[2], "path"=>$a[3],"originalvalue"=>$a[4],"newvalue"=>"","status"=>"remove"));	
	}
	
	return $masterArray;
}


function _pr($a) {
	echo "<pre>";
	print_r($a);
	echo "</pre>";
}
?>