jQuery(document).ready(function() {
	jQuery('#upload button.btn.upload').click(function () {	
		if(jQuery("input#fileToUpload").val()) {
			var file_data = $('#fileToUpload').prop('files')[0];   
			var form_data = new FormData();                  
			form_data.append('file', file_data);
			jQuery.ajax({
			  type: "POST",
			  url: "ajax/uploadfile.php",
			  dataType: 'text',
              cache: false,
              contentType: false,
              processData: false,
			  data: form_data,
			})
			.done(function( msg ) {						
			  jQuery("#upload .result").html(msg);						  			  
			});	
		} else {
		}
	});
	
	jQuery('#compare .btn.submit').click(function () {	
			jQuery.ajax({
			  type: "POST",
			  url: "ajax/configtable.php",
			  data: { homef:"", forif: jQuery('.selectimportfile').val()  }
			})
			.done(function( msg ) {						
			  jQuery('div#compare-table').html(msg);						  
			  jQuery('div#importchanges').addClass("active");
			});					
	});
	
	jQuery("div#export .btn.submit").click(function() {
		jQuery('div#export .result').html("");
		if (!jQuery('#configfilename').val()) {
			jQuery('div#export .result').html("Please enter valid file name.");
		} else {
			jQuery('div#export .result').html("Generating the file...");
			jQuery.ajax({
			  type: "POST",
			  url: "ajax/exportdbtable.php",
			  data: { magemodel:jQuery('#magemodel').val(),forif:jQuery('#configfilename').val() }
			})
			.done(function( msg ) {
			  var downloadlink = jQuery(location).attr('href')+msg;
			  var message = "File Created. <a href='"+downloadlink+"' download='"+jQuery('#configfilename').val()+"'>Click here to download.</a>"
			  jQuery('div#export .result').html(message);
			});
		}
	});
	

	jQuery("li.exp").click(function (){	
		jQuery('.page-row.control-panel ul li').removeClass("active");
		jQuery(".page-row.action-block .column").removeClass("active");
		jQuery('div#export').addClass("active");
		jQuery(this).addClass("active");
	});
	jQuery("li.upl").click(function (){
		jQuery('.page-row.control-panel ul li').removeClass("active");
		jQuery(".page-row.action-block .column").removeClass("active");
		jQuery('div#upload').addClass("active");
		jQuery(this).addClass("active");
	});
	jQuery("li.imp").click(function (){
		jQuery('.page-row.control-panel ul li').removeClass("active");
		jQuery(".page-row.action-block .column").removeClass("active");
		jQuery('div#compare').addClass("active");
		jQuery(this).addClass("active");
	});	
	jQuery("li.his").click(function (){
		jQuery('.page-row.control-panel ul li').removeClass("active");
		jQuery(".page-row.action-block .column").removeClass("active");
		jQuery('div#history').addClass("active");
		jQuery(this).addClass("active");
	}); 
		
	jQuery("#compare-table").on("click",".table .row", function (){		
		jQuery(".table .row").removeClass("active");
		jQuery(this).toggleClass("active");
	});
});
			