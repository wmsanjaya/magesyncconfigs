<?php
	require 'class/racc.php';
	require 'config.php';
	require '../app/Mage.php';
	umask(0);
	Mage::app();
?>
<html>
	<head>
		<title>Magento Import Config Table</title>
		<link href="css/style.css" rel="stylesheet" type="text/css"/>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
		<script src="js/script.js"></script>
	</head>
	<body>		
		<section class="header">
		<div class="page-row title"> 
			<h1>Magento Import Config Table</h1>
		</div>
		<div class="page-row control-panel">
			<ul>
				<li class="exp active">Export</li>
				<li class="upl">Upload</li>
				<li class="imp">Compare and Import</li>	
				<li class="his"><a href="<?php echo BASEURL;?>/history.php">History</a></li>						
			</ul>
		</div>
		</section>
		<section class="body">
			<div class="page-row action-block">
				<div id="export" class="column half tabbody active">
					<h2>Export Config Table To a File</h2>
					<p>This can be used to backup or export magento settings to a file so you can transfer it to another Magento website.</p>
					<div class="form-body">
						<div class="element"><label for="brachname">File Name:</label>
							<?php //print_r(Mage::getConfig()->getNode('global/models')); ?>
							<select id="magemodel" name="magemodel">								
								<option value="coreconf">Core Config</option>															
								<option value="products">Products</option>															
								<option value="category">Categories</option>															
							</select>
						</div>
						<div class="element"><label for="brachname">File Name:</label><input type="textbox" name="configfilename" id="configfilename" /></div>
						<div class="element submit"><button class="btn submit">Export</div>
						<div class="result"></div>
						<div class="">Download old exported files <a href="exportedfiles.php">here</a></div>
					</div>
				</div>
				<div id="upload" class="column half forg tabbody">
					<h2>Upload Config Table File</h2>
					<div class="form-body">
						<div class="element"><label for="fileToUpload">Select image to upload:</label></div>
						<div class="element"><input type="file" name="fileToUpload" id="fileToUpload" accept=".xml"></div>
						<div class="element submit"><button class="btn upload">Upload</button></div>
						<div class="result"></div>
					</div>
				</div>			
				<div id="compare" class="column half tabbody">
					<h2>Select file to import and compare</h2>
					<div class="form-body">
						<form id="compareform" action="comp.php" method="get">
						<select name="file" class="selectimportfile">
						<?php 
						foreach (glob("xmlfiles/upload_*.xml") as $filename) {
							echo "<option value=".$filename.">".$filename."</option>";
						}
						?>
						</select>
						<div class="element submit"><button type="submit" class="btn submit">Compare</div>
						</form>
					</div>
				<div id="history" class="column half tabbody">
					<h2>History</h2>
					<div class="form-body">				
					</div>					
				</div>
			</div>
		</section>		
	</body>
</html>